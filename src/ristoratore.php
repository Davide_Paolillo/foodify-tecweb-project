<?php
//Dichiarazione variabili per server
$servername="localhost";
$username ="root";
$password ="";
$database = "foodifydb";

$conn =new mysqli($servername, $username, $password, $database);
?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Google's Material Design Icons -->
    <link rel="stylesheet" href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <link rel="stylesheet" href="/foodify-tecweb-project/css/ristoratore.css">
    <script src="jquery-3.3.1.min.js"></script>

    <title>RistoratoreHomePage</title>
  </head>

  <body>

        <!-- INSERIRE Navbar standard del progetto con LOGOUT??-->
    <div class="menu">
      <?php require 'navbar.php'; ?>
    </div>


    <div class="container">
      <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
          <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Profile</a>
          <a class="nav-item nav-link" id="nav-ordini-tab" data-toggle="tab" href="#nav-ordini" role="tab" aria-controls="nav-ordini" aria-selected="false">Order</a>
          <a class="nav-item nav-link" id="nav-nuovoprodotto-tab" data-toggle="tab" href="#nav-nuovoprodotto" role="tab" aria-controls="nav-nuovoprodotto" aria-selected="false">New Product</a>
          <a class="nav-item nav-link" id="nav-listino-tab" data-toggle="tab" href="#nav-listino" role="tab" aria-controls="nav-listino" aria-selected="false">List</a>
        </div>
      </nav>
      <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
          <div class="media">
            <img src="..." class="align-self-start mr-3" alt="...">
            <div class="media-body">
              <h5 class="mt-0">Restaurant profile</h5>
              <p>**DESCRIZIONE RISTORANTE DA PRENDERE DAL DB**</p>
              <div class="item">
                <p class="item-name">Restaurant name</p>
                <p class="item-description">**nome ristorante dal DB**</p>
                <hr>
                <p class="item-name">Owner name</p>
                <p class="item-description">**nome propietario dal DB**</p>
                <hr>
                <p class="item-name">Owner surname</p>
                <p class="item-description">**cognome propietario dal DB**</p>
                <hr>
                <p class="item-name">Restaurant telephone</p>
                <p class="item-description">**numero ristorante dal DB**</p>
                <hr>
                <p class="item-name">Restaurant email</p>
                <p class="item-description">**email ristorante dal DB**</p>
                <hr>
                <p class="item-name">Restaurant P_IVA</p>
                <p class="item-description">**p_iva ristorante dal DB**</p>
              </div>
            </div>
          </div>

        </div>


        <div class="tab-pane fade" id="nav-ordini" role="tabpanel" aria-labelledby="nav-ordini-tab">
          <?php
            if($conn->connect_errno){
          ?>
            <p>Failed to connect to MySQL: <?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
          <?php
          }
          else{
            $query_sql="SELECT `idOrdine`, `data`, `ora`, `prezzo_tot`, `cliente_email`, FROM ORDINE";
            $result = $conn->query($query_sql);
            if($result !== false){
            ?>
          <table class="ordertable">
            <thead>
              <th scope="col">Id ordine</th>
              <th scope="col">Data</th>
              <th scope="col">Orario</th>
              <th scope="col">Prezzo</th>
              <th scope="col">Cliente</th>
              <th scope="col">Stato</th>
            </thead>
            <tbody>
              <?php
              if($result->num_rows > 0){
                while ($row = $result->fetch_assoc()) {
                ?>
                <tr>
                  <td><?php echo $row["idOrdine"]; ?></td>
                  <td><?php echo $row["data"]; ?></td>
                  <td><?php echo $row["ora"]; ?></td>
                  <td><?php echo $row["prezzo_tot"]; ?></td>
                  <td><?php echo $row["cliente_email"]; ?></td>
                  <td>
                    <select name="stato">
                    <option value="attesa">attesa</option>
                    <option value="accettato">accettato</option>
                    <option value="consegnato">consegnato</option>
                    </select>
                  </td>
                </tr>
                 <?php
                }
              }
            ?>
            </tbody>
          </table>
          <?php
			}
			else{
		?>
			<p>Errore nell'interrogazione</p>
		<?php
    }
    //Chiusura connessione con db
			$conn->close();
		}
	?>
    </div>

        <div class="tab-pane fade" id="nav-nuovoprodotto" role="tabpanel" aria-labelledby="nav-nuovoprodotto-tab">
          <form action="new_product.php" method="post">
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-2 col-form-label">Product id</label>
              <div class="col-sm-10">
                <input type="number" class="form-control" id="inputidprodotto" placeholder="product id" name="idp">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-2 col-form-label">List id</label>
              <div class="col-sm-10">
                <input type="number" class="form-control" id="inputidlistino" placeholder="list id" name="idl">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-2 col-form-label">Product name</label>
              <div class="col-sm-10">
                <input type="nome" class="form-control" id="inputnome" placeholder="name" name="name">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-2 col-form-label">Product price ($)</label>
              <div class="col-sm-10">
                <input type="number" class="form-control" id="inputprezzo" placeholder="price" name="price">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-2 col-form-label">Product type</label>
              <div class="col-sm-10">
                <input type="nome" class="form-control" id="inputtipo" placeholder="type" name="type">
              </div>
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Ingredients</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" name="ing"></textarea>
            </div>
            <div class="form-group row">
              <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Insert</button>
              </div>
            </div>
        </div>


        <div class="tab-pane fade" id="nav-listino" role="tabpanel" aria-labelledby="nav-listino-tab">
          <form class="form-inline my-2 my-lg-0" method="post" action="cercainlista.php">
                <input class="form-control mr-sm-2" type="search" placeholder="Cerca" aria-label="Search" name="cerca">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cerca</button>
          </form>
          <?php
            if($conn->connect_errno){
          ?>
          	<p>Failed to connect to MySQL: <?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
          <?php
          }
          else{
            $query_sql="SELECT `idProdotto`, `nome`, `prezzo`, `tipo`, `descrizione`, `idListino` FROM PRODOTTO";
            $result = $conn->query($query_sql);
            if($result !== false){
            ?>
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">Id prodotto</th>
                  <th scope="col">Nome prodotto</th>
                  <th scope="col">Prezzo ($)</th>
                  <th scope="col">Tipo</th>
                  <th scope="col">Descrizione</th>
                  <th scope="col">Id listino</th>
                </tr>
              </thead>
              <tbody>
            <?php
              if($result->num_rows > 0){
                while ($row = $result->fetch_assoc()) {
                  ?>
                  <tr>
                    <td><?php echo $row["idProdotto"]; ?></td>
                    <td><?php echo $row["nome"]; ?></td>
                    <td><?php echo $row["prezzo"]; ?></td>
                    <td><?php echo $row["tipo"]; ?></td>
                    <td><?php echo $row["descrizione"]; ?></td>
                    <td><?php echo $row["idListino"]; ?></td>
                  </tr>
                  <?php
                }
              }
            ?>
              </tbody>
            </table>
          <?php
            }
            else{
          ?>
            <p>Errore nell'interrogazione</p>
          <?php
            }
          //Chiusura connessione con db
            $conn->close();
          }
        ?>
      </div>

    </div>
  </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>
