<!DOCTYPE html>
<html>
<head>
  <title>Payment</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

  <!-- Google's Material Design Icons -->
  <link rel="stylesheet" href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

  <link rel="stylesheet" href="/foodify-tecweb-project/css/checkout.css">
</head>
<body>
    <div class="container-fluid" id="back_arrow">
        <div class="nav-item" style="float:left">
          <br>
          <span onclick="history.go(-1);"><a class="navbar-menu"><i class="material-icons" style="color:black">arrow_back</i></a></span>
        </div>
    </div>

  <main class="page payment-page">
    <section class="payment-form dark">
      <div class="container">
        <div class="block-heading">
          <h2>Checkout</h2>
          <p>Pay with Credit Card, PayPal or Cash on Delivery</p>
        </div>
        <form>
          <div class="products">
            <h3 class="title">Cart</h3>
            <div class="item">
              <span class="price">$200</span>
              <p class="item-name">Product 1</p>
              <p class="item-description">Description</p>
            </div>
            <div class="item">
              <span class="price">$120</span>
              <p class="item-name">Product 2</p>
              <p class="item-description">Description</p>
            </div>
            <div class="total">Total<span class="price">$320</span></div>
          </div>

          <div class="container-fluid">
            <div class="panel-group" id="accordion">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Pay with Card</a>
                  </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse"> <!-- class="panel-collapse collapse in" to open directly this tab -->
                  <div class="panel-body"  <div class="card-details">
                    <h3 class="title">Credit or Debit Card</h3>
                    <div class="row">
                      <div class="form-group col-sm-7">
                        <label for="card-holder">Card Holder</label>
                        <input id="card-holder" type="text" class="form-control" placeholder="Card Holder" aria-label="Card Holder" aria-describedby="basic-addon1">
                      </div>
                      <div class="form-group col-sm-5">
                        <label for="">Expiration Date</label>
                        <div class="input-group expiration-date">
                          <input type="text" class="form-control" placeholder="MM" aria-label="MM" aria-describedby="basic-addon1">
                          <span class="date-separator">/</span>
                          <input type="text" class="form-control" placeholder="YY" aria-label="YY" aria-describedby="basic-addon1">
                        </div>
                      </div>
                      <div class="form-group col-sm-8">
                        <label for="card-number">Card Number</label>
                        <input id="card-number" type="text" class="form-control" placeholder="Card Number" aria-label="Card Holder" aria-describedby="basic-addon1">
                      </div>
                      <div class="form-group col-sm-4">
                        <label for="cvc">CVC</label>
                        <input id="cvc" type="text" class="form-control" placeholder="CVC" aria-label="Card Holder" aria-describedby="basic-addon1">
                      </div>
                      <div class="form-group col-sm-12">
                        <button type="button" class="btn btn-primary btn-block">Place Order</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">PayPal</a>
                </h4>
              </div>
              <div id="collapse2" class="panel-collapse collapse">
                <div class="panel-body">
                  <h3>Pay with PayPal</h3>
                  <p>Click on the button below and you'll be redirected to PayPal Website to complete your payment</p>
                  <a href="#"><img src="/img/paypal-checkout-button.png" class="img-responsive" alt="PayPal Checkout Button"></a>
                </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Cash on Delivery</a>
                  </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>Pay with Cash when the order gets delivered.</p>
                    <br/>
                    <div class="form-group col-sm-12">
                      <button type="button" class="btn btn-primary btn-block">Place Order</button>
                    </div>
                  </div>
                  </div>
                </div>
              </div>
            </div>
        </form>
      </div>
    </section>
  </main>
</body>
</body>
</html>
