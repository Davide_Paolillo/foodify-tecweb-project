<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Contact Us</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <!-- My CSS -->
  <link rel="stylesheet" href="/foodify-tecweb-project/css/style.css">

  <!-- Google's Material Design Icons -->
  <link rel="stylesheet" href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<body>
  <div class="menu">
    <?php require 'navbar.php'; ?>
  </div>

  <div class="container contact-form mx-auto">
    <form method="post">
      <h3>Send Us a Message</h3>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <input type="text" name="txtName" class="form-control" placeholder="Your Name *" value="" />
          </div>
          <div class="form-group">
            <input type="text" name="txtEmail" class="form-control" placeholder="Your Email *" value="" />
          </div>
          <div class="form-group">
            <input type="text" name="txtPhone" class="form-control" placeholder="Your Phone Number *" value="" />
          </div>
          <div class="form-group">
            <textarea name="txtMsg" class="form-control" placeholder="Your Message *" style="width: 100%; height: 150px;"></textarea>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <input type="submit" name="btnSubmit" class="btnContact" value="Send Message" />
          </div>
        </div>
      </div>
    </form>
  </div>
  <link rel="stylesheet" href="/foodify-tecweb-project/css/contact_us.css">
  <!-- Side Menu and Cart JS-->
  <script src="/foodify-tecweb-project/js/sidemenu.js"></script>
  <script src="/foodify-tecweb-project/js/sidecart.js"></script>
</body>
</html>
