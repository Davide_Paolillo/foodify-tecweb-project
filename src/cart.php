<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
  	<link rel="stylesheet" href="/foodify-tecweb-project/css/cart.css"> <!-- Cart CSS -->
    <script src="/foodify-tecweb-project/js/sidecart.js"></script>
    <title>Cart 4</title>

  </head>
  <body>
    <div id="cd-cart" class="container-fluid">
		<h2>Cart</h2>
		<ul class="cd-cart-items">
			<li>
				<span class="cd-qty">1x</span> Product Name
				<div class="cd-price">$9.99</div>
				<a href="#0" class="cd-item-remove cd-img-replace">Remove</a>
			</li>

			<li>
				<span class="cd-qty">2x</span> Product Name
				<div class="cd-price">$19.98</div>
				<a href="#0" class="cd-item-remove cd-img-replace">Remove</a>
			</li>

			<li>
				<span class="cd-qty">1x</span> Product Name
				<div class="cd-price">$9.99</div>
				<a href="#0" class="cd-item-remove cd-img-replace">Remove</a>
			</li>
		</ul> <!-- cd-cart-items -->

		<div class="cd-cart-total">
			<h3>Total: <span id="total-price">$39.96</span></h3>
		</div> <!-- cd-cart-total -->
    <div class="container-fluid" id="div-checkout-btn">
      <a onclick="breakout()" class="checkout-btn">Checkout</a>
    </div>

	</div> <!-- cd-cart -->
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  </body>
</html>
