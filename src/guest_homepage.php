<?php
session_start();
$cookie_name = "user";
$cookie_value = "";
setcookie($cookie_name. $cookie_value, time() + (86400 * 30 * 12), "/");
session_unset();
?>
<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  <!-- My CSS -->
  <link rel="stylesheet" href="/foodify-tecweb-project/css/style.css">
  <!-- FontAwesome Icons -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <!-- Google's Material Design Icons -->
  <link rel="stylesheet" href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <title>Foodify - HomePage</title>
</head>
<body>
  <div class="menu">
    <?php require 'navbar.php'; ?>
  </div>

  <!-- CARUSEL ITEM (photo) -->
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active" id="item1">
        <img src="/foodify-tecweb-project/img/pasta2.jpg" class="d-block w-100 img-fluid" style="text-align: center" alt="Pasta">
        <div class="carousel-caption d-none d-md-block">
            <h3>GET YOUR FOOD AT UNIVERSITY!</h3>
            <p>If you don't get it in 30 minutes, FREE DELIVERY</p>
        </div>
      </div>
      <div class="carousel-item" id="item2">
        <img src="/foodify-tecweb-project/img/hamburger2.jpg" class="d-block w-100 img-fluid" style="text-align: center" alt="Hamburger">
        <div class="carousel-caption d-none d-md-block">
            <h3>GET YOUR FOOD AT UNIVERSITY!</h3>
            <p>If you don't get it in 30 minutes, FREE DELIVERY</p>
        </div>
      </div>
      <div class="carousel-item" id="item3">
        <img src="/foodify-tecweb-project/img/piadina2.jpg" class="d-block w-100 img-fluid" style="text-align: center" alt="Piadina Romagnola">
        <div class="carousel-caption d-none d-md-block">
            <h3>GET YOUR FOOD AT UNIVERSITY!</h3>
            <p>If you don't get it in 30 minutes, FREE DELIVERY</p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <div class="free-delivery-food" style="text-align: center">
    <p><span id="free-delivery-color-change">Free Delivery</span><br> if you don't get your food within 30 Minutes!</p>
  </div>

  <div class="order-now-button" style="text-align: center">
    <a href="/foodify-tecweb-project/src/all_restaurants.php"><img src="/foodify-tecweb-project/img/OrderNow.png" class="img-fluid" alt="Order Now"></a>
  </div>

  <div class="trust-icons" style="text-align: center">
    <i class="material-icons" style="color:black; font-size:75px;">fastfood</i>
    <h5>Choose Your Restaurant</h5>
    <i class="material-icons" style="color:black; font-size:75px;">attach_money</i>
    <h5>Pay with PayPal, Card or Cash On Delivery</h5>
    <i class="material-icons" style="color:black; font-size:75px;">local_shipping</i>
    <h5>~15 Minutes for Delivery</h5>
  </div>

  <div class="container" id="footer-homepage">
    <?php require 'footer.php'; ?>
  </div>

  <!-- Google Fonts API -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Staatliches">
</body>
</html>
