    <nav class="navbar navbar-expand-md navbar-light bg-white sticky-top">
      <div class="container-fluid">
          <div class="nav-item">
            <span onclick="toggleNav()"><a class="navbar-menu"><i class="material-icons" style="color:black">menu</i></a></span>
          </div>

          <div class="navbar-header">
            <a class="navbar-brand-logo" href="/foodify-tecweb-project/index.php"><img src="/foodify-tecweb-project/img/logo.png"></a>
          </div>

          <div class="nav-item">
            <span onclick="toggleCart()"><a class="navbar-cart-button"><i class="material-icons float:right" style="color:black">shopping_cart</i></a></span>
          </div>
      </div>
    </nav>

    <!-- Side Menu Settings -->
   <div id="sidemenu" class="sidenav">
      <a href="/foodify-tecweb-project/src/login.php">Login/Register</a>
    </div>

    <div id="sidecart" class="sidenav-cart">
      <iframe src="/foodify-tecweb-project/src/cart.php" width="100%" height="100%" sandbox="allow-top-navigation allow-scripts allow-forms"></iframe>
    </div>

    <!-- Google's Material Design Icons -->
    <link rel="stylesheet" href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Side Menu and Cart JS -->
    <script src="/foodify-tecweb-project/js/sidemenu.js"></script>
    <script src="/foodify-tecweb-project/js/sidecart.js"></script>
    <script src="/foodify-tecweb-project/js/cart_redirect.js"></script>
    <link rel="stylesheet" href="/foodify-tecweb-project/css/navbar.css">
