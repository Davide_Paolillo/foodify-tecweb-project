    <div class="container" style="text-align: center">
      <footer class="footer-page">
        <div class="row mt-3 black-text">
          <div class="col-md-3 col-lg-3 col-xl-3 mb-4">
            <h6 class="text-uppercase font-weight-bold">Foodify</h6>
            <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
            <p>Hungry? Try Foodify and let us deliver your favourite food at the University!</p>
          </div>
          <div class="col-md-4 col-lg-3 col-xl-6 mx-auto mb-4">
            <h6 class="text-uppercase font-weight-bold">Useful Links</h6>
            <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
            <p>
              <a class="black-text" href="/foodify-tecweb-project/index.php">Homepage</a>
            </p>
            <p>
              <a class="black-text" href="/foodify-tecweb-project/src/help.php">Help</a>
            </p>
            <p>
              <a class="black-text" href="/foodify-tecweb-project/src/contact_us.php">Contact Us</a>
            </p>
          </div>

          <div class="col-md-4 col-lg-4 col-xl-3 mx-auto mb-md-0 mb-4">
            <h6 class="text-uppercase font-weight-bold">Contacts</h6>
            <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
            <p>
              <i class="fas fa-home mr-3"></i> Cesena (FC) 47521, Italy</p>
            <p>
              <i class="fas fa-envelope mr-3"></i> support@foodify.com</p>
            <p>
              <i class="fas fa-phone mr-3"></i> +39 1234567890</p>
            <p>
              <i class="fas fa-print mr-3"></i> +39 0987654321</p>
          </div>
        </div>

        <div class="footer-copyright text-center text-black-50" id="footer-copyright">© 2019 Copyright:
          <p>Foodify</p>
        </div>
      </footer>
    </div>

    <style>
      .footer-page{
        margin-bottom: 30px;
        border-top: 0.5px solid grey;
        padding-top: 10px;
      }
    </style>
