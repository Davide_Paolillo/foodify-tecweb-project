<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Foodify - Login</title>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/foodify-tecweb-project/css/login.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="/foodify-tecweb-project/js/login-register.js"></script>

    <!-- Google's Material Design Icons -->
    <link rel="stylesheet" href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <div class="container-fluid" id="back_arrow">
        <div class="nav-item" style="float:left">
          <br>
          <span onclick="history.back();"><a class="navbar-menu"><i class="material-icons" style="color:black">arrow_back</i></a></span>
        </div>
    </div>
    <div class="container" id="login-div">
    	<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-6">
								<a href="#" class="active" id="login-form-link">Login</a>
							</div>
							<div class="col-xs-6">
								<a href="#" id="register-form-link">Register</a>
							</div>
						</div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<form id="login-form" action="/foodify-tecweb-project/controller/login_check.php" method="post" role="form" style="display: block;">
									<div class="form-group">
										<input type="text" name="email" id="email-login" tabindex="1" class="form-control" placeholder="Email Address" value="">
									</div>
									<div class="form-group">
										<input type="password" name="password" id="password-login" tabindex="2" class="form-control" placeholder="Password">
									</div>
									<div class="form-group text-center">
										<input type="checkbox" tabindex="3" class="" name="remember" id="remember">
										<label for="remember"> Remember Me</label>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
											</div>
										</div>
									</div>
								</form>
								<form id="register-form" action="/foodify-tecweb-project/controller/register.php" method="post" role="form" style="display: none;">
								<!--<div class="form-group">
										<input type="text" name="username" id="username-register" tabindex="1" class="form-control" placeholder="Username" value="">
									</div>
                -->
                  <div class="form-group" id="a">
                    <input type="text" name="name" id="name-register" tabindex="1" class="form-control" placeholder="Name" value="">
                  </div>
                  <div class="form-group" id="b">
                    <input type="text" name="surname" id="surname-register" tabindex="1" class="form-control" placeholder="Surname" value="">
                  </div>
                  <div class="form-group" id="c">
                    <input type="text" name="CF" id="cf-register" tabindex="1" class="form-control" placeholder="Codice Fiscale" value="">
                  </div>
                  <div class="form-group" id="d">
                    <input type="text" name="piva" id="piva-register" tabindex="1" class="form-control" placeholder="P.IVA" value="">
                  </div>
                  <div class="form-group" id="e">
                    <input type="text" name="phone" id="phone-register" tabindex="1" class="form-control" placeholder="Phone Number" value="">
                  </div>
                  <div class="form-group" id="f">
                    <input type="text" name="restaurant-name" id="restaurant-name-register" tabindex="1" class="form-control" placeholder="Restaurant Name" value="">
                  </div>
									<div class="form-group">
										<input type="email" name="email" id="email-register" tabindex="1" class="form-control" placeholder="Email Address" value="">
									</div>
									<div class="form-group">
										<input type="password" name="password" id="password-register" tabindex="2" class="form-control" placeholder="Password">
									</div>
									<div class="form-group">
										<input type="password" name="confirm-password" id="confirm-password-register" tabindex="2" class="form-control" placeholder="Confirm Password">
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Register Now">
											</div>
										</div>
									</div>
                  <div class="radio-check" style="text-align: center">
                    <span id="who-are-you-text"><p>Who are you?</p></span>
                    <label class="radio-inline"><input type="radio" class="checkbox" id="stud" name="optradio" checked value="student">Student</label>
                    <label class="radio-inline"><input type="radio" class="checkbox" id="rest" name="optradio" value="restaurant">Restaurant</label>
                    <label class="radio-inline"><input type="radio" class="checkbox" id="delm" name="optradio" value="delivery-man">Delivery Man</label>
                  </div>
								</form>
                <script>
                  $(document).ready(function () {
                    $(".checkbox").click(function () {
                      if($(this).attr('id') == "rest") {
                        $('#a').show();
                        $('#b').show();
                        $('#c').show();
                        $('#d').show();
                        $('#e').show();
                        $('#f').show();
                      } else if ($(this).attr('id') == "delm") {
                        $('#a').show();
                        $('#b').show();
                        $('#c').hide();
                        $('#d').hide();
                        $('#e').hide();
                        $('#f').hide();
                      } else if ($(this).attr('id') == "stud") {
                        $('#a').hide();
                        $('#b').hide();
                        $('#c').hide();
                        $('#d').hide();
                        $('#e').hide();
                        $('#f').hide();
                      }
                    });
                  });
                </script>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
  </body>
</html>
