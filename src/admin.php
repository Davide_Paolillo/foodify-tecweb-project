<?php
//Dichiarazione variabili per server
$servername="localhost";
$username ="root";
$password ="";
$database = "foodifydb";

$conn =new mysqli($servername, $username, $password, $database);
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Google's Material Design Icons -->
    <link rel="stylesheet" href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <link rel="stylesheet" href="/foodify-tecweb-project/css/admin.css">

    <title>AdminHomePage</title>
  </head>

  <body>

    <!-- INSERIRE Navbar standard del progetto con LOGOUT??-->
  <div class="menu">
  <?php require 'navbar.php'; ?>
  </div>


<div class="container">
  <nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
      <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Utenti</a>
      <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Ristoratori</a>
      <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Fattorini</a>
    </div>
  </nav>
  <div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
      <?php
      	if ($conn->connect_errno) {
      	?>
      		<p>Failed to connect to MySQL: <?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
      	<?php
        }
        else{
        	$query_sql="SELECT email, nome, cognome FROM CLIENTE";
      		$result = $conn->query($query_sql);
    		if($result !== false){
        ?>
        <table>
          <thead>
            <tr>
              <th scope="col">Email</th>
              <th scope="col">Name</th>
              <th scope="col">Surname</th>
            </tr>
          </thead>
          <tbody>
        <?php
          if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()){
        ?>
          <tr>
            <th scope="row"><?php echo $row["email"]; ?></th>
            <td><?php echo $row["nome"]; ?></td>
            <td><?php echo $row["cognome"]; ?></td>
          </tr>
          <?php
					}
				}
			?>
        </tbody>
      </table>
      <?php
      	}
    		else{
  		?>
        <p>Errore nell'interrogazione</p>
      <?php
  			}
      	 //Chiusura connessione con db
      	  $conn->close();
      }
  	?>


  </div>
    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
      <?php
        if ($conn->connect_errno) {
        ?>
          <p>Failed to connect to MySQL: <?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
        <?php
        }
        else{
          $query_sql="SELECT email, nome, cognome, CF, P_IVA, telefono, ristorante FROM FORNITORE";
          $result = $conn->query($query_sql);
        if($result !== false){
        ?>
        <table>
          <thead>
            <tr>
              <th scope="col">Email</th>
              <th scope="col">Name</th>
              <th scope="col">Surname</th>
              <th scope="col">CF</th>
              <th scope="col">P_IVA</th>
              <th scope="col">Telephone</th>
              <th scope="col">Restaurant</th>
            </tr>
          </thead>
          <tbody>
        <?php
          if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()){
        ?>
          <tr>
            <th scope="row"><?php echo $row["email"]; ?></th>
            <td><?php echo $row["nome"]; ?></td>
            <td><?php echo $row["cognome"]; ?></td>
            <td><?php echo $row["CF"]; ?></td>
            <td><?php echo $row["P_IVA"]; ?></td>
            <td><?php echo $row["telefono"]; ?></td>
            <td><?php echo $row["ristorante"]; ?></td>
          </tr>
          <?php
          }
        }
      ?>
        </tbody>
      </table>
      <?php
        }
        else{
      ?>
        <p>Errore nell'interrogazione</p>
      <?php
        }
         //Chiusura connessione con db
          $conn->close();
      }
      ?>
  </div>


    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
      <?php
      	if ($conn->connect_errno) {
      	?>
      		<p>Failed to connect to MySQL: <?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
      	<?php
        }
        else{
        	$query_sql="SELECT email, nome, cognome FROM FATTORINO";
      		$result = $conn->query($query_sql);
    		if($result !== false){
        ?>
        <table>
          <thead>
            <tr>
              <th scope="col">Email</th>
              <th scope="col">Name</th>
              <th scope="col">Surname</th>
            </tr>
          </thead>
          <tbody>
        <?php
          if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()){
        ?>
          <tr>
            <th scope="row"><?php echo $row["email"]; ?></th>
            <td><?php echo $row["nome"]; ?></td>
            <td><?php echo $row["cognome"]; ?></td>
          </tr>
          <?php
					}
				}
			?>
        </tbody>
      </table>
      <?php
      	}
    		else{
  		?>
        <p>Errore nell'interrogazione</p>
      <?php
  			}
      	 //Chiusura connessione con db
      	  $conn->close();
      }
  	?>
    </div>
  </div>
</div>



  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
