<?php
//Dichiarazione variabili per server
$servername="localhost";
$username ="root";
$password ="";
$database = "foodifydb";

$conn =new mysqli($servername, $username, $password, $database);
?>

<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

  <!-- FontAwesome Icons -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

  <!-- Google's Material Design Icons -->
  <link rel="stylesheet" href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script type="text/javascript" src="bottone_select.js"></script>

  </script>

  <title>Fattorino - HomePage</title>
</head>
<body>

  <div class="menu">

    <?php require 'navbar.php'; ?>

  </div>

  <div class="container">
    <?php
		if ($conn->connect_errno) {
		?>
			<p>Failed to connect to MySQL: <?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
		<?php
		}
		else{
			$query_sql="SELECT `idOrdine`, `data`, `ora`, `prezzo_tot`, `fornitore_email`, `cliente_email`, `idAula` FROM ORDINE";
			$result = $conn->query($query_sql);
			if($result !== false){
			?>
      <table class="table table-hover">
        <thead>
          <th scope="col">Id ordine</th>
          <th scope="col">Data</th>
          <th scope="col">Orario</th>
          <th scope="col">Prezzo</th>
          <th scope="col">Fornitore</th>
          <th scope="col">Cliente</th>
          <th scope="col">Aula</th>
        </thead>
        <tbody>
          <?php
          if($result->num_rows > 0){
            while ($row = $result->fetch_assoc()) {
            ?>
            <tr>
              <td><?php echo $row["idOrdine"]; ?></td>
              <td><?php echo $row["data"]; ?></td>
              <td><?php echo $row["ora"]; ?></td>
              <td><?php echo $row["prezzo_tot"]; ?></td>
              <td><?php echo $row["fornitore_email"]; ?></td>
              <td><?php echo $row["cliente_email"]; ?></td>
              <td><?php echo $row["idAula"]; ?></td>
              <td>
                <select name="stato">
                <option value="attesa">attesa</option>
                <option value="accettato">accettato</option>
                <option value="consegnato">consegnato</option>
                </select>
              </td>
            </tr>
             <?php
            }
          }
        ?>
        </tbody>
      </table>
      <?php
  }
  else{
?>
  <p>Errore nell'interrogazione</p>
<?php
}
//Chiusura connessione con db
  $conn->close();
}
?>
    </div>



<script type="text/javascript" src="/js/login-register.js">


</script>
  </body>
</html>

<style>

.container{
  margin-top: 30px;
}

</style>
