var isCartOpen = false;
var isMobile = false;

if (window.matchMedia('(max-width: 576px)').matches) {
  isMobile = true;
}

function openCart(){
  if(isMobile===false){
    $("body").css({"position": "absolute", "overflow": "auto", "margin-right" : "30%", "width" : "70%", "transition": "0.5s"});
    document.getElementById("sidecart").style.width = "30%";
  } else {
    document.getElementById("sidecart").style.width = "250px";
  }
}

function closeCart(){
  if (isMobile===false){
    $("body").css({"position": "auto", "overflow-x": "hidden", "margin-right" : "0", "width" : "100%", "transition": "0.5s"});
  }
  document.getElementById("sidecart").style.width = "0px";
}

function toggleCart(){
  if (isCartOpen) {
    closeCart();
    isCartOpen = false;
  } else {
    openCart();
    isCartOpen = true;
  }
}


function breakout() {
  if (window.top != window.self) {
    window.top.location = "/foodify-tecweb-project/src/checkout.php";
  }
}
