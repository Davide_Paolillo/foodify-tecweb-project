$(document).ready(function() {
  var sellers = [];
  $.ajax({
    url:"/foodify-tecweb-project/controller/all_restaurant.php",
    success:function(result){
      var json = JSON.parse(result);
      for(var i = 0; i < json.length; i++){
        sellers[i] = json[i];
      }

      for(var i = 0; i < sellers.length; i++) {
        $(".card-group").append("<div class='card w-100'><h5 class='card-title'>" + sellers[i].ristorante +
        "</h5> <img class='card-img-top' src="+ sellers[i].immagine +
        " alt='image of the restaurant'><div class='card-body'><p class='card-text'>"+sellers[i].descrizione+
        "</p><p class='card-text'><small class='text-muted'><i class='fas fa-phone' title='phone number'></i> "+ sellers[i].telefono+ '</small></p><a href="#" class="btn btn-primary">Order Now</a></div></div>');
      }
    }
  });
});
