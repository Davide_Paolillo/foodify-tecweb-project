<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "foodifydb";

$query_select_listino = "SELECT idListino FROM listino ORDER BY idListino";
$query_select_carrello = "SELECT idCarrello FROM carrello ORDER BY idCarrello";

$response = "not executed";

$conn = new mysqli($servername, $username, $password, $database);
//Control wether the connection has been established succesfully or not
if ($conn -> connect_errno){
  echo "Failed to connect to MySQL: (" . $conn -> connect_errno - ")" . $conn->connect_error;
}

$result = $conn->query($query_select_listino);
if($result!==FALSE){
  if($result->num_rows > 0){
    while($row = $result->fetch_assoc()){
      $MaxListino = $row["idListino"];
    }
    $idListino = $MaxListino + 1;
  }
  else{
    $idListino = 0000000;
  }
}

$result = $conn->query($query_select_carrello);
if($result!== FALSE){
  if($result->num_rows > 0){
    while($row = $result->fetch_assoc()){
      $MaxCarrello = $row["idCarrello"];
    }
    $idCarrello = $MaxCarrello + 1;
  }
  else{
    $idCarrello = 0000000;
  }
}

if ($_POST['optradio'] == 'student'){
  $rest = ""; //Restaurant
  $delm = ""; //DeliveryMan
  $stud = "Studente";
  $stmt = $conn->prepare("INSERT INTO cliente VALUES (?, ?)");
  $stmt->bind_param("ss", $_POST['email'], $_POST['username']);
  if($stmt->execute()){
    $response .= "ok";
  }
  $stmt = $conn->prepare("INSERT INTO utente VALUES (?, ?, ?, ?, ?)");
  $stmt->bind_param("sssss", $_POST['email'], $_POST['password'], $rest, $delm, $stud);
  if($stmt->execute()){
    $response .= "ok";
  }
  $stmt = $conn->prepare("INSERT INTO carrello VALUES (?, ?)");
  $stmt -> bind_param("is", $idCarrello, $_POST['email']);
  if($stmt->execute()){
    $response .= "ok";
  }
}

if ($_POST['optradio'] == 'restaurant'){
  $stud = "";
  $delm = "";
  $rest = "Ristorante";
  $stmt = $conn->prepare("INSERT INTO fornitore VALUES (?, ?, ?, ?, ?, ?, ?)");
  $stmt -> bind_param("sssssss", $_POST['name'], $_POST['surname'], $_POST['CF'],
  $_POST['piva'], $_POST['phone'], $_POST["restaurant-name"], $_POST['email']);
  if($stmt->execute()){
    $response .= "ok";
  }
  $stmt = $conn->prepare("INSERT INTO utente VALUES (?, ?, ?, ?, ?)");
  $stmt -> bind_param("sssss", $_POST['email'], $_POST['password'], $delm, $rest, $stud);
  if($stmt->execute()){
    $response .= "ok";
  }
  $stmt = $conn->prepare("INSERT INTO listino VALUES (?, ?)");
  $stmt -> bind_param("is", $idListino, $_POST['email']);
  if($stmt->execute()){
    $response = "ok";
  }
}

if ($_POST['optradio'] == 'delivery-man'){
  $stud = "";
  $rest = "";
  $delm = "Delivery-Man";
  $stmt = $conn->prepare("INSERT INTO fattorino VALUES (?, ?, ?, ?)");
  $stmt -> bind_param("ssss",  $_POST['username'], $_POST['surname'], $_POST['email'], $_POST['email_forn']);
  if($stmt -> execute()){
    $response .= "ok";
  }
  $stmt = $conn->prepare("INSERT INTO utente VALUES (?, ?, ?, ?, ?)");
  $stmt -> bind_param("sssss", $_POST['email'], $_POST['password'], $delm, $stud, $rest);
  if($stmt -> execute()){
    $response .= "ok";
  }
  $response .= "ok";
}

echo $response; //Print the response
//Close connection with DB
$stmt->close();
$conn->close();

header('Location: /foodify-tecweb-project/src/login.php');
?>
