<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "foodifydb";

$auleUni = array();

$conn = new mysqli($servername, $username, $password, $database);
if($conn -> connect_error){
  die("Connection to the Database Failed: " . $conn->connect_error);
}

$stmt = $conn->prepare("SELECT * FROM aula");
$stmt -> execute();
$result = $stmt->get_result();
$output = array(); //Array vuoto per memorizzare l'output della query
while($row = $result->fetch_assoc()){
  $output[] = $row;
}
foreach ($output as $key => $value) {
  $auleUni = array();
  $aula['idAula'] = $output[$key]['idAula'];
  $aula['locazione'] = $output[$key]['locazione'];
  $auleUni[$key] = $aula;
}
echo json_encode($auleUni);
$stmt->close();
//Invio la query e chiudo la connessione con il DB
//Come sempre, se vuoi modificare, prima chiedi a Cri
?>
